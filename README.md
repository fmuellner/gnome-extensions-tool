# gnome-extensions-tool
gnome-extensions-tool has moved to the [gnome-shell] repository.

Please file your issues and merge requests in its [bug-tracker].

[gnome-shell]: https://gitlab.gnome.org/GNOME/gnome-shell/
[bug-tracker]: https://gitlab.gnome.org/GNOME/gnome-shell/issues
